/*
 *  GeoServer-Manager - Simple Manager Library for GeoServer
 *  
 *  Copyright (C) 2007,2016 GeoSolutions S.A.S.
 *  http://www.geo-solutions.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package it.geosolutions.geoserver.rest;

import it.geosolutions.geoserver.rest.http.GeoServerRestAuthenticator;
import it.geosolutions.geoserver.rest.http.UsernamePasswordAuthenticator;
import it.geosolutions.geoserver.rest.manager.GeoServerRESTAbstractManager;
import it.geosolutions.geoserver.rest.manager.GeoServerRESTResourceManager;
import it.geosolutions.geoserver.rest.manager.GeoServerRESTSecurityManager;
import it.geosolutions.geoserver.rest.manager.GeoServerRESTStoreManager;
import it.geosolutions.geoserver.rest.manager.GeoServerRESTStructuredGridCoverageReaderManager;
import it.geosolutions.geoserver.rest.manager.GeoServerRESTStyleManager;
import it.geosolutions.geoserver.rest.manager.GeoServerRESTCustomService;

import java.net.URL;

/**
 * <i>The</i> single entry point to all of geoserver-manager functionality.
 *
 * Instance this one, and use getters to use different components. These are:
 * <ul>
 * <li>getReader() simple, high-level access methods.
 * <li>getPublisher() simple, high-level pubhish methods.
 * <li>get<i>Foo</i>Manager, full-fledged management of catalog objects.
 * </ul>
 *
 * @author Oscar Fonts
 * @author Carlo Cancellieri - carlo.cancellieri@geo-solutions.it
 * @version $Id: $
 */
public class GeoServerRESTManager extends GeoServerRESTAbstractManager {

    private final GeoServerRESTPublisher publisher;
    private final GeoServerRESTReader reader;

    private final GeoServerRESTStoreManager storeManager;
    private final GeoServerRESTStyleManager styleManager;
    
    private final GeoServerRESTStructuredGridCoverageReaderManager structuredGridCoverageReader;
    
    private final GeoWebCacheREST geoWebCacheRest;
    private final GeoServerRESTResourceManager resourceManager;
    private final GeoServerRESTSecurityManager securityManager;
    
    /**
     * Default constructor.
     *
     * Indicates connection parameters to remote GeoServer instance.
     *
     * @param restURL GeoServer REST API endpoint
     * @param username GeoServer REST API authorized username
     * @param password GeoServer REST API password for the former username
     * @throws java.lang.IllegalArgumentException {@link GeoServerRESTAbstractManager#GeoServerRESTAbstractManager(URL, String, String)}
     */
    public GeoServerRESTManager(URL restURL, String username, String password)
            throws IllegalArgumentException {
        this(restURL, new UsernamePasswordAuthenticator(username, password));
    }

    /**
     * Default constructor.
     *
     * Indicates connection parameters to remote GeoServer instance.
     *
     * @param restURL GeoServer REST API endpoint
     * @param auth GeoServer REST API authenticator
     * @throws java.lang.IllegalArgumentException {@link GeoServerRESTAbstractManager#GeoServerRESTAbstractManager(URL, String, String)}
     */
    public GeoServerRESTManager(URL restURL, GeoServerRestAuthenticator auth)
            throws IllegalArgumentException {
        super(restURL, auth);

        // Internal publisher and reader, provide simple access methods.
        publisher = new GeoServerRESTPublisher(restURL.toString(), auth);
        reader = new GeoServerRESTReader(restURL, auth);
        structuredGridCoverageReader = new GeoServerRESTStructuredGridCoverageReaderManager(restURL, auth);
        storeManager = new GeoServerRESTStoreManager(restURL, auth);
        styleManager = new GeoServerRESTStyleManager(restURL, auth);
        geoWebCacheRest = new GeoWebCacheREST(restURL, auth);
        resourceManager = new GeoServerRESTResourceManager(restURL, auth);
        securityManager = new GeoServerRESTSecurityManager(restURL, auth);
    }

    /**
     * <p>Getter for the field <code>publisher</code>.</p>
     *
     * @return a {@link it.geosolutions.geoserver.rest.GeoServerRESTPublisher} object.
     */
    public GeoServerRESTPublisher getPublisher() {
        return publisher;
    }

    /**
     * <p>Getter for the field <code>reader</code>.</p>
     *
     * @return a {@link it.geosolutions.geoserver.rest.GeoServerRESTReader} object.
     */
    public GeoServerRESTReader getReader() {
        return reader;
    }

    /**
     * <p>Getter for the field <code>storeManager</code>.</p>
     *
     * @return a {@link it.geosolutions.geoserver.rest.manager.GeoServerRESTStoreManager} object.
     */
    public GeoServerRESTStoreManager getStoreManager() {
        return storeManager;
    }

    /**
     * <p>Getter for the field <code>styleManager</code>.</p>
     *
     * @return a {@link it.geosolutions.geoserver.rest.manager.GeoServerRESTStyleManager} object.
     */
    public GeoServerRESTStyleManager getStyleManager() {
        return styleManager;
    }

    /**
     * <p>Getter for the field <code>structuredGridCoverageReader</code>.</p>
     *
     * @return a {@link it.geosolutions.geoserver.rest.manager.GeoServerRESTStructuredGridCoverageReaderManager} object.
     */
    public GeoServerRESTStructuredGridCoverageReaderManager getStructuredGridCoverageReader() {
        return structuredGridCoverageReader;
    }

    /**
     * <p>Getter for the field <code>geoWebCacheRest</code>.</p>
     *
     * @return a {@link it.geosolutions.geoserver.rest.GeoWebCacheREST} object.
     */
    public GeoWebCacheREST getGeoWebCacheRest() {
        return geoWebCacheRest;
    }
    
    /**
     * <p>Getter for the field <code>resourceManager</code>.</p>
     *
     * @return a {@link it.geosolutions.geoserver.rest.manager.GeoServerRESTResourceManager} object.
     */
    public GeoServerRESTResourceManager getResourceManager() {
        return resourceManager;
    }
    
    /**
     * <p>Getter for the field <code>securityManager</code>.</p>
     *
     * @return a {@link it.geosolutions.geoserver.rest.manager.GeoServerRESTSecurityManager} object.
     */
    public GeoServerRESTSecurityManager getSecurityManager() {
        return securityManager;
    }
    
    /**
     * <p>Getter for the field <code>custom service</code>.</p>
     *
     * @return a {@link it.geosolutions.geoserver.rest.manager.GeoServerRESTCustomService} object.
     */
    public GeoServerRESTCustomService getCustomService(String service) {
        return new GeoServerRESTCustomService(gsBaseUrl, auth, service);
    }

}
