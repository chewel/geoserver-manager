package it.geosolutions.geoserver.rest.decoder;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import it.geosolutions.geoserver.rest.decoder.utils.JDOMBuilder;
import org.jdom.Element;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * 网格集
 */
public class RESTGridset {
    protected final Element element;

    public RESTGridset(Element element) {
        this.element = element;
    }

    public static RESTGridset build(String response) {
        if(StrUtil.isBlank(response)) {
            return null;
        }
        Element element = JDOMBuilder.buildElement(response);
        if(element != null) {
            return new RESTGridset(element);
        }
        return null;
    }

    /**
     * 获取网格集名称
     * @return a String
     */
    public String getName() {
        return element.getChildText("name");
    }

    public boolean getAlignTopLeft() {
        return Boolean.parseBoolean(element.getChildText("alignTopLeft"));
    }

    /**
     * Y坐标优先，还是X坐标优先，默认X坐标优先
     * @return a boolean
     */
    public boolean getYCoordinateFirst() {
        return Boolean.parseBoolean(element.getChildText("yCoordinateFirst"));
    }

    /**
     * 每个坐标单位的米数
     * @return a double
     */
    public double getMetersPerUnit() {
        return Double.parseDouble(element.getChildText("metersPerUnit"));
    }

    /**
     * 像素尺寸
     * @return a double
     */
    public double getPixelSize() {
        return Double.parseDouble(element.getChildText("pixelSize"));
    }

    /**
     * 切片的高度
     * @return an int
     */
    public int getTileHeight() {
        return Integer.parseInt(element.getChildText("tileHeight"));
    }

    /**
     * 切片的宽度
     * @return an int
     */
    public int getTileWidth() {
        return Integer.parseInt(element.getChildText("tileWidth"));
    }

    /**
     * 网格集的参考系
     * @return an int
     */
    public int getSRS() {
        return Integer.parseInt(element.getChild("srs").getChildText("number"));
    }

    /**
     * 范围坐标[minx,miny,maxx.maxy]
     * @return a array
     */
    public double[] getCoords() {
        Element extent = element.getChild("extent");
        Element coord = extent.getChild("coords");
        List<Element> children = coord.getChildren();
        return new double[]{
                Double.parseDouble(children.get(0).getText()),
                Double.parseDouble(children.get(1).getText()),
                Double.parseDouble(children.get(2).getText()),
                Double.parseDouble(children.get(3).getText())
        };
    }

    /**
     * 每级网格的分辨率
     * @return a double array
     */
    public double[] getResoutions() {
        Element resolutions = element.getChild("resolutions");
        List<Element> children = resolutions.getChildren();
        return children.stream().mapToDouble(s -> Double.parseDouble(s.getText())).toArray();
    }

    /**
     * 每个网格集级别的比例名称
     * @return a string array
     */
    public String[] getScaleNames() {
        Element resolutions = element.getChild("scaleNames");
        List<Element> children = resolutions.getChildren();
        List<String> scaleNames = new ArrayList<>();
        children.forEach(s->scaleNames.add(s.getText()));
        return ArrayUtil.toArray(scaleNames, String.class);
    }
}
