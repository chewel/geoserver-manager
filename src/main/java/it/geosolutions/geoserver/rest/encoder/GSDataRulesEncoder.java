/*
 *  GeoServer-Manager - Simple Manager Library for GeoServer
 *  
 *  Copyright (C) 2007,2011 GeoSolutions S.A.S.
 *  http://www.geo-solutions.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package it.geosolutions.geoserver.rest.encoder;

import it.geosolutions.geoserver.rest.encoder.utils.PropertyXMLEncoder;
import it.geosolutions.geoserver.rest.manager.GeoServerRESTSecurityManager;

import java.util.Collection;
import java.util.Iterator;

import org.jdom.Element;

/**
  * 
  */
public class GSDataRulesEncoder extends PropertyXMLEncoder {
    private final static String RULES = "rules";
    private final static String RULE = "rule";    
    private final static String RESOURCE = "resource";


    /**
     * <p>
     * Constructor for GSWorkspaceEncoder.
     * </p>
     */
    public GSDataRulesEncoder() {
        super(RULES);
    }

    /**
     * Add the name to this workspace
     *
     * @param name a {@link java.lang.String} object.
     * @throws java.lang.IllegalStateException if name is already set
     */
    public void addRule(final String workspace, final String layer, final GeoServerRESTSecurityManager.RuleType type,
            final Collection<String> roles) {
        Element ruleElement = new Element(RULE);
        ruleElement.setAttribute(RESOURCE, GeoServerRESTSecurityManager.resource(workspace, layer, type));
        if (roles.isEmpty()) {
            ruleElement.setText(GeoServerRESTSecurityManager.ANY);
        } else {
            Iterator<String> it = roles.iterator();
            StringBuffer sb = new StringBuffer(it.next());
            while (it.hasNext()) {
                sb.append(",");
                sb.append(it.next());
            }
            ruleElement.setText(sb.toString());
        }
        addContent(ruleElement);
    }

}
