package it.geosolutions.geoserver.rest.encoder.gridsets;

import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.ObjUtil;
import it.geosolutions.geoserver.rest.encoder.utils.PropertyXMLEncoder;
import org.jdom.Content;
import org.jdom.Element;

public class GSGridSetEncoder extends PropertyXMLEncoder {
    private final Element  srsElm = new Element("srs");
    private final Element extentElm = new Element("extent");
    private final Element resolutionsElm = new Element("resolutions");
    private final Element scaleNamesElm = new Element("scaleNames");

    public GSGridSetEncoder(String name) {
        this(name, null);
    }

    public GSGridSetEncoder(String name, String description) {
        super("gridSet");
        if(CharSequenceUtil.isBlank(name)) {
            throw new RuntimeException("网格集名称不能为空");
        }

        this.set("name", name);
        this.set("yCoordinateFirst", "false");
        this.set("alignTopLeft", "false");

        if(CharSequenceUtil.isNotBlank(description)) {
            set("description", description);
        }

        addContent(srsElm);
        addContent(extentElm);
        addContent(resolutionsElm);
        addContent(scaleNamesElm);
    }

    public String getName() {
        return get("name").getText();
    }

    public void setTileSize(int width, int height) {
        if(width > 2048) {
            width = 2048;
        }
        if(width < 16) {
            width = 16;
        }
        if(height > 2048) {
            height = 2048;
        }
        if(height < 16) {
            height = 16;
        }
        set("tileHeight", String.valueOf(height));
        set("tileWidth", String.valueOf(width));
    }

    public void setPixelSize(double pixelSize) {
        if(ObjUtil.isNotNull(pixelSize)) {
            set("pixelSize", String.valueOf(pixelSize));
        }
    }

    public void setMetersPerUnit(double metersPerUnit) {
        if(ObjUtil.isNotNull(metersPerUnit)) {
            set("metersPerUnit", String.valueOf(metersPerUnit));
        }
    }

    public void setSRS(int srs) {
        srsElm.setContent(new Element("number").setText(String.valueOf(srs)));
    }

    public void setCoords(double minx, double miny, double maxx, double maxy) {
        Element element = new Element("coords")
                .addContent(new Element("double").setText(String.valueOf(minx)))
                .addContent(new Element("double").setText(String.valueOf(miny)))
                .addContent(new Element("double").setText(String.valueOf(maxx)))
                .addContent(new Element("double").setText(String.valueOf(maxy)));
        extentElm.setContent(element);
    }

    public void addLevel(String scaleName, double value) {
        if(CharSequenceUtil.isNotBlank(scaleName) && ObjUtil.isNotNull(value)) {
            resolutionsElm.addContent(new Element("double").setText(String.valueOf(value)));
            scaleNamesElm.addContent(new Element("string").setText(scaleName));
        }
    }
}
