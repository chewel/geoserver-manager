package it.geosolutions.geoserver.rest.http;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;

public interface GeoServerRestAuthenticator {
    
    void setAuth(HttpClient client, HttpMethod method) throws IOException;

}
