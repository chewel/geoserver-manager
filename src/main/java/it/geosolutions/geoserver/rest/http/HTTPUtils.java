/*
 *  GeoServer-Manager - Simple Manager Library for GeoServer
 *  
 *  Copyright (C) 2007,2011 GeoSolutions S.A.S.
 *  http://www.geo-solutions.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package it.geosolutions.geoserver.rest.http;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSON;
import net.sf.json.JSONSerializer;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.EntityEnclosingMethod;
import org.apache.commons.httpclient.methods.FileRequestEntity;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Low level HTTP utilities.
 *
 * @author niels
 * @version $Id: $
 */
public class HTTPUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(HTTPUtils.class);
        
    public static GeoServerRestAuthenticator NO_AUTH = new GeoServerRestAuthenticator() {
        @Override
        public void setAuth(HttpClient client, HttpMethod method) throws URIException {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Not setting credentials to access to " + method.getURI());
            }
        }
    };
    
    public static String enc(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return str;
        }
    }

    /**
     * Performs an HTTP GET on the given URL. <BR>
     * Basic auth is used if both username and pw are not null.
     *
     * @param url The URL where to connect to.
     * @param username Basic auth credential. No basic auth if null.
     * @param pw Basic auth credential. No basic auth if null.
     * @return The HTTP response as a String if the HTTP response code was 200
     *         (OK).
     */
    public static String get(String url, GeoServerRestAuthenticator auth) {

        GetMethod httpMethod = null;
        HttpClient client = new HttpClient();
        HttpConnectionManager connectionManager = client.getHttpConnectionManager();
        try {
            httpMethod = new GetMethod(url);
            auth.setAuth(client, httpMethod);
            connectionManager.getParams().setConnectionTimeout(5000);
            int status = client.executeMethod(httpMethod);
            if (status == HttpStatus.SC_OK) {
                InputStream is = httpMethod.getResponseBodyAsStream();
                String response = IOUtils.toString(is);
                IOUtils.closeQuietly(is);
                if (response.trim().length() == 0) { // sometime gs rest fails
                    LOGGER.warn("ResponseBody is empty");
                    return null;
                } else {
                    return response;
                }
            } else {
                LOGGER.info("(" + status + ") " + HttpStatus.getStatusText(status) + " -- " + url);
            }
        } catch (ConnectException e) {
            LOGGER.info("Couldn't connect to [" + url + "]");
        } catch (IOException e) {
            LOGGER.info("Error talking to [" + url + "]", e);
        } finally {
            if (httpMethod != null)
                httpMethod.releaseConnection();
            connectionManager.closeIdleConnections(0);
        }

        return null;
    }
    
    /**
     * Performs an HTTP GET on the given URL. <BR>
     * Basic auth is used if both username and pw are not null.
     *
     * @param url The URL where to connect to.
     * @param username Basic auth credential. No basic auth if null.
     * @param pw Basic auth credential. No basic auth if null.
     * @return The HTTP response as a Stream if the HTTP response code was 200
     *         (OK).
     */
    public static InputStream getAsStream(String url, GeoServerRestAuthenticator auth) {

        GetMethod httpMethod = null;
        HttpClient client = new HttpClient();
        HttpConnectionManager connectionManager = client.getHttpConnectionManager();
        try {
            auth.setAuth(client, httpMethod);
            httpMethod = new GetMethod(url);
            connectionManager.getParams().setConnectionTimeout(5000);
            int status = client.executeMethod(httpMethod);
            if (status == HttpStatus.SC_OK) {
                return httpMethod.getResponseBodyAsStream();
            } else {
                LOGGER.info("(" + status + ") " + HttpStatus.getStatusText(status) + " -- " + url);
            }
        } catch (ConnectException e) {
            LOGGER.info("Couldn't connect to [" + url + "]");
        } catch (IOException e) {
            LOGGER.info("Error talking to [" + url + "]", e);
        } finally {
            if (httpMethod != null)
                httpMethod.releaseConnection();
            connectionManager.closeIdleConnections(0);
        }

        return null;
    }

    /**
     * Executes a request using the GET method and parses the result as a json object.
     *
     * @return The result parsed as json.
     * @param url a {@link java.lang.String} object.
     * @param username a {@link java.lang.String} object.
     * @param pw a {@link java.lang.String} object.
     * @throws java.lang.Exception if any.
     */
    public static JSON getAsJSON(String url, GeoServerRestAuthenticator auth) throws Exception {
        String response = get(url, auth);
        return json(response);
    }
    
    /**
     * <p>json</p>
     *
     * @param content a {@link java.lang.String} object.
     * @return a {@link net.sf.json.JSON} object.
     */
    public static JSON json(String content) {
        return JSONSerializer.toJSON(content);
    }
    
    /**
     * PUTs a File to the given URL. <BR>
     * Basic auth is used if both username and pw are not null.
     *
     * @param url The URL where to connect to.
     * @param file The File to be sent.
     * @param contentType The content-type to advert in the PUT.
     * @param username Basic auth credential. No basic auth if null.
     * @param pw Basic auth credential. No basic auth if null.
     * @return the HTTP response or null on errors.
     * @return the HTTP response or null on errors.
     */
    public static String put(String url, File file, String contentType, GeoServerRestAuthenticator auth) {
        return put(url, new FileRequestEntity(file, contentType), auth);
    }

    /**
     * PUTs a String to the given URL. <BR>
     * Basic auth is used if both username and pw are not null.
     *
     * @param url The URL where to connect to.
     * @param content The content to be sent as a String.
     * @param contentType The content-type to advert in the PUT.
     * @param contentType The content-type to advert in the PUT.
     * @param username Basic auth credential. No basic auth if null.
     * @param pw Basic auth credential. No basic auth if null.
     * @return the HTTP response or null on errors.
     * @return the HTTP response or null on errors.
     */
    public static String put(String url, String content, String contentType, GeoServerRestAuthenticator auth) {
        try {
            return put(url, new StringRequestEntity(content, contentType, null), auth);
        } catch (UnsupportedEncodingException ex) {
            LOGGER.error("Cannot PUT " + url, ex);
            return null;
        }
    }

    /**
     * PUTs a String representing an XML document to the given URL. <BR>
     * Basic auth is used if both username and pw are not null.
     *
     * @param url The URL where to connect to.
     * @param content The XML content to be sent as a String.
     * @param username Basic auth credential. No basic auth if null.
     * @param pw Basic auth credential. No basic auth if null.
     * @return the HTTP response or null on errors.
     * @return the HTTP response or null on errors.
     */
    public static String putXml(String url, String content, GeoServerRestAuthenticator auth) {
        return put(url, content, "text/xml", auth);
    }

    /**
     * PUTs a String representing an JSON Object to the given URL. <BR>
     * Basic auth is used if both username and pw are not null.
     *
     * @param url The URL where to connect to.
     * @param content The JSON Object to be sent as a String.
     * @param username Basic auth credential. No basic auth if null.
     * @param pw Basic auth credential. No basic auth if null.
     * @return the HTTP response or null on errors.
     * @return the HTTP response or null on errors.
     */
    public static String putJson(String url, String content, GeoServerRestAuthenticator auth) {
        return put(url, content, "application/json", auth);
    }
    
    /**
     * Performs a PUT to the given URL. <BR>
     * Basic auth is used if both username and pw are not null.
     *
     * @param url The URL where to connect to.
     * @param requestEntity The request to be sent.
     * @param username Basic auth credential. No basic auth if null.
     * @param pw Basic auth credential. No basic auth if null.
     * @return the HTTP response or null on errors.
     * @return the HTTP response or null on errors.
     */
    public static String put(String url, RequestEntity requestEntity, GeoServerRestAuthenticator auth) {
        return send(new PutMethod(url), url, requestEntity, auth);
    }

    /**
     * POSTs a File to the given URL. <BR>
     * Basic auth is used if both username and pw are not null.
     *
     * @param url The URL where to connect to.
     * @param file The File to be sent.
     * @param contentType The content-type to advert in the POST.
     * @param username Basic auth credential. No basic auth if null.
     * @param pw Basic auth credential. No basic auth if null.
     * @return the HTTP response or null on errors.
     * @return the HTTP response or null on errors.
     */
    public static String post(String url, File file, String contentType, GeoServerRestAuthenticator auth) {
        return post(url, new FileRequestEntity(file, contentType), auth);
    }

    /**
     * POSTs a String to the given URL. <BR>
     * Basic auth is used if both username and pw are not null.
     *
     * @param url The URL where to connect to.
     * @param content The content to be sent as a String.
     * @param contentType The content-type to advert in the POST.
     * @param contentType The content-type to advert in the POST.
     * @param username Basic auth credential. No basic auth if null.
     * @param pw Basic auth credential. No basic auth if null.
     * @return the HTTP response or null on errors.
     * @return the HTTP response or null on errors.
     */
    public static String post(String url, String content, String contentType, 
            GeoServerRestAuthenticator auth) {
        try {
            return post(url, new StringRequestEntity(content, contentType, null), auth);
        } catch (UnsupportedEncodingException ex) {
            LOGGER.error("Cannot POST " + url, ex);
            return null;
        }
    }

    /**
     * POSTs a list of files as attachments to the given URL. <BR>
     * Basic auth is used if both username and pw are not null.
     *
     * @param url The URL where to connect to.
     * @param dir The folder containing the attachments.
     * @param username Basic auth credential. No basic auth if null.
     * @param pw Basic auth credential. No basic auth if null.
     * @return the HTTP response or null on errors.
     * @return the HTTP response or null on errors.
     */
    public static String postMultipartForm(String url, File dir, GeoServerRestAuthenticator auth) {
        try {
            List<Part> parts = new ArrayList<Part>();
            for (File f : dir.listFiles()) {
                parts.add(new FilePart(f.getName(), f));
            }
            MultipartRequestEntity multipart = new MultipartRequestEntity(
                    parts.toArray(new Part[parts.size()]), new PostMethod().getParams());

            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            multipart.writeRequest(bout);
            
            return post(url, multipart, auth);
        } catch (Exception ex) {
            LOGGER.error("Cannot POST " + url, ex);
            return null;
        }
    }
    
    /**
     * POSTs a String representing an XML document to the given URL. <BR>
     * Basic auth is used if both username and pw are not null.
     *
     * @param url The URL where to connect to.
     * @param content The XML content to be sent as a String.
     * @param username Basic auth credential. No basic auth if null.
     * @param pw Basic auth credential. No basic auth if null.
     * @return the HTTP response or null on errors.
     * @return the HTTP response or null on errors.
     */
    public static String postXml(String url, String content, GeoServerRestAuthenticator auth) {
        return post(url, content, "text/xml", auth);
    }

    /**
     * POSTs a String representing an JSON Object to the given URL. <BR>
     * Basic auth is used if both username and pw are not null.
     *
     * @param url The URL where to connect to.
     * @param content The JSON content to be sent as a String.
     * @param username Basic auth credential. No basic auth if null.
     * @param pw Basic auth credential. No basic auth if null.
     * @return the HTTP response or null on errors.
     * @return the HTTP response or null on errors.
     */
    public static String postJson(String url, String content, GeoServerRestAuthenticator auth) {
        return post(url, content, "application/json", auth);
    }
    
    /**
     * Performs a POST to the given URL. <BR>
     * Basic auth is used if both username and pw are not null.
     *
     * @param url The URL where to connect to.
     * @param requestEntity The request to be sent.
     * @param username Basic auth credential. No basic auth if null.
     * @param pw Basic auth credential. No basic auth if null.
     * @return the HTTP response or null on errors.
     * @return the HTTP response or null on errors.
     */
    public static String post(String url, RequestEntity requestEntity, GeoServerRestAuthenticator auth) {
        return send(new PostMethod(url), url, requestEntity, auth);
    }

    /**
     * Send an HTTP request (PUT or POST) to a server. <BR>
     * Basic auth is used if both username and pw are not null.
     * <P>
     * Only
     * <UL>
     * <LI>200: OK</LI>
     * <LI>201: ACCEPTED</LI>
     * <LI>202: CREATED</LI>
     * </UL>
     * are accepted as successful codes; in these cases the response string will
     * be returned.
     * 
     * @return the HTTP response or null on errors.
     */
    private static String send(final EntityEnclosingMethod httpMethod, String url,
                               RequestEntity requestEntity, GeoServerRestAuthenticator auth) {
        HttpClient client = new HttpClient();
        HttpConnectionManager connectionManager = client.getHttpConnectionManager();
        try {
            auth.setAuth(client, httpMethod);
            connectionManager.getParams().setConnectionTimeout(5000);
            if (requestEntity != null)
                httpMethod.setRequestEntity(requestEntity);
            int status = client.executeMethod(httpMethod);

            InputStream responseBody;
            switch (status) {
            case HttpURLConnection.HTTP_OK:
            case HttpURLConnection.HTTP_CREATED:
            case HttpURLConnection.HTTP_ACCEPTED:
                String response = IOUtils.toString(httpMethod.getResponseBodyAsStream());
                // LOGGER.info("================= POST " + url);
                if (LOGGER.isInfoEnabled())
                    LOGGER.info("HTTP " + httpMethod.getStatusText() + ": " + response);
                return response;
            default:
                responseBody = httpMethod.getResponseBodyAsStream();
                LOGGER.warn("Bad response: code[" + status + "]" + " msg[" + httpMethod.getStatusText() + "]"
                            + " url[" + url + "]" + " method[" + httpMethod.getClass().getSimpleName()
                            + "]: " + (responseBody != null ? IOUtils.toString(responseBody) : ""));
                return null;
            }
        } catch (ConnectException e) {
            LOGGER.info("Couldn't connect to [" + url + "]");
            return null;
        } catch (IOException e) {
            LOGGER.error("Error talking to " + url + " : " + e.getLocalizedMessage());
            return null;
        } finally {
            if (httpMethod != null)
                httpMethod.releaseConnection();
            connectionManager.closeIdleConnections(0);
        }
    }

    /**
     * <p>delete</p>
     *
     * @param url a {@link java.lang.String} object.
     * @param user a {@link java.lang.String} object.
     * @param pw a {@link java.lang.String} object.
     * @return a boolean.
     */
    public static boolean delete(String url, GeoServerRestAuthenticator auth) {

        DeleteMethod httpMethod = null;
        HttpClient client = new HttpClient();
        HttpConnectionManager connectionManager = client.getHttpConnectionManager();
        try {
            httpMethod = new DeleteMethod(url);
            auth.setAuth(client, httpMethod);
            connectionManager.getParams().setConnectionTimeout(5000);
            int status = client.executeMethod(httpMethod);
            String response = "";
            if (status == HttpStatus.SC_OK) {
                InputStream is = httpMethod.getResponseBodyAsStream();
                response = IOUtils.toString(is);
                IOUtils.closeQuietly(is);
                if (response.trim().equals("")) { 
                    if (LOGGER.isTraceEnabled())
                        LOGGER.trace("ResponseBody is empty (this may be not an error since we just performed a DELETE call)");
                    return true;
                }
                if (LOGGER.isDebugEnabled())
                    LOGGER.debug("(" + status + ") " + httpMethod.getStatusText() + " -- " + url);
                return true;
            } else {
                LOGGER.info("(" + status + ") " + httpMethod.getStatusText() + " -- " + url);
                LOGGER.info("Response: '" + response + "'");
            }
        } catch (ConnectException e) {
            LOGGER.info("Couldn't connect to [" + url + "]");
        } catch (IOException e) {
            LOGGER.info("Error talking to [" + url + "]", e);
        } finally {
            if (httpMethod != null)
                httpMethod.releaseConnection();
            connectionManager.closeIdleConnections(0);
        }

        return false;
    }

    /**
     * <p>httpPing</p>
     *
     * @param url a {@link java.lang.String} object.
     * @param username a {@link java.lang.String} object.
     * @param pw a {@link java.lang.String} object.
     * @return a boolean.
     */
    public static boolean httpPing(String url, GeoServerRestAuthenticator auth) {

        GetMethod httpMethod = null;
        HttpClient client = new HttpClient();
        HttpConnectionManager connectionManager = client.getHttpConnectionManager();
        try {
            httpMethod = new GetMethod(url);
            auth.setAuth(client, httpMethod);
            connectionManager.getParams().setConnectionTimeout(2000);
            int status = client.executeMethod(httpMethod);
            if (status != HttpStatus.SC_OK) {
                LOGGER.warn("PING failed at '" + url + "': (" + status + ") " + httpMethod.getStatusText());
                return false;
            } else {
                return true;
            }
        } catch (ConnectException e) {
            return false;
        } catch (IOException e) {
            LOGGER.error(e.getLocalizedMessage(),e);
            return false;
        } finally {
            if (httpMethod != null)
                httpMethod.releaseConnection();
            connectionManager.closeIdleConnections(0);
        }
    }

    /**
     * Used to query for REST resources.
     *
     * @param url The URL of the REST resource to query about.
     * @param username a {@link java.lang.String} object.
     * @param pw a {@link java.lang.String} object.
     * @return true on 200, false on 404.
     * @throws java.lang.RuntimeException on unhandled status or exceptions.
     */
    public static boolean exists(String url, GeoServerRestAuthenticator auth) {

        GetMethod httpMethod = null;
        HttpClient client = new HttpClient();
        HttpConnectionManager connectionManager = client.getHttpConnectionManager();
        try {
            httpMethod = new GetMethod(url);
            auth.setAuth(client, httpMethod);
            connectionManager.getParams().setConnectionTimeout(2000);
            int status = client.executeMethod(httpMethod);
            switch (status) {
            case HttpStatus.SC_OK:
                return true;
            case HttpStatus.SC_NOT_FOUND:
                return false;
            default:
                throw new RuntimeException("Unhandled response status at '" + url + "': (" + status + ") "
                                           + httpMethod.getStatusText());
            }
        } catch (ConnectException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (httpMethod != null)
                httpMethod.releaseConnection();
            connectionManager.closeIdleConnections(0);
        }
    }

    /**
     * <p>decurtSlash</p>
     *
     * @param geoserverURL a {@link java.lang.String} object.
     * @return recursively remove ending slashes
     */
    public static String decurtSlash(String geoserverURL) {
        if (geoserverURL!=null && geoserverURL.endsWith("/")) {
            geoserverURL = decurtSlash(geoserverURL.substring(0, geoserverURL.length() - 1));
        }
        return geoserverURL;
    }
    
    /**
     * <p>append</p>
     *
     * @param str a string array
     * @return create a StringBuilder appending all the passed arguments
     */
    public static StringBuilder append(String ... str){
        if (str==null){
            return null;
        }
        
        StringBuilder buf=new StringBuilder();
        for (String s: str){
            if (s!=null)
                buf.append(s);
        }
        return buf;
    }
    
    /**
     * Wrapper for {@link #append(String...)}
     *
     * @param base base URL
     * @param str strings to append
     * @return the base URL with parameters attached
     */
    public static StringBuilder append(URL base, String ... str){
        if (str==null){
            return append(base.toString());
        }
        
        StringBuilder buf=new StringBuilder(base.toString());
        for (String s: str){
            if (s!=null)
                buf.append(s);
        }
        return buf;
    }

}
