package it.geosolutions.geoserver.rest.manager;

import java.net.URL;

import it.geosolutions.geoserver.rest.http.GeoServerRestAuthenticator;
import it.geosolutions.geoserver.rest.http.HTTPUtils;

public class GeoServerRESTCustomService extends GeoServerRESTAbstractManager {
	
	private final String serviceName;
    
	public GeoServerRESTCustomService(URL baseurl, GeoServerRestAuthenticator auth, String serviceName) {
		super(baseurl, auth);
		this.serviceName = serviceName;
	}
	
	public String get(String request, String... requestParameters) {
		StringBuilder sUrl = new StringBuilder(gsBaseUrl.toString())
				.append("/").append(serviceName)
				.append("/").append(request);	
		if (requestParameters.length > 0) {
			sUrl.append("?");
		}
		for (int i = 0; i < requestParameters.length - 1; i += 2) {
			sUrl.append(requestParameters[i])
				.append("=").append(requestParameters[i + 1]);
		}
		
        return HTTPUtils.get(sUrl.toString(), auth);
	}
	
	public void post(String request, String contentType, String data) {
		StringBuilder sUrl = new StringBuilder(gsBaseUrl.toString())
				.append("/").append(serviceName)
				.append("/").append(request);		
		
		HTTPUtils.post(sUrl.toString(), data, contentType, auth);
	}
	
	public void put(String request, String contentType, String data) {
		StringBuilder sUrl = new StringBuilder(gsBaseUrl.toString())
				.append("/").append(serviceName)
				.append("/").append(request);		
		
		HTTPUtils.put(sUrl.toString(), data, contentType, auth);
	}
    
    

}
