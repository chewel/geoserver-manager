/*
 *  GeoServer-Manager - Simple Manager Library for GeoServer
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package it.geosolutions.geoserver.rest.manager;

import it.geosolutions.geoserver.rest.http.HTTPUtils;
import it.geosolutions.geoserver.rest.decoder.RESTDataRules;
import it.geosolutions.geoserver.rest.encoder.GSDataRulesEncoder;
import it.geosolutions.geoserver.rest.http.GeoServerRestAuthenticator;

import java.net.URL;

public class GeoServerRESTSecurityManager extends GeoServerRESTAbstractManager {
    
    public final static String ANY = "*";
    
    public static enum RuleType {
        R, W, A
    }
    
    public static String resource(String workspace, String layer, RuleType type) {
        return workspace + "." + layer + "." + type.name().toLowerCase();
    }
    
    public GeoServerRESTSecurityManager(URL restURL, GeoServerRestAuthenticator auth)
            throws IllegalArgumentException {
        super(restURL, auth);
    }

    private String SECURITY_PATH = "/rest/security";
    private String DATA_PATH = "/acl/layers";
    
    private String urlFromPath(String path) {
        return HTTPUtils.append(gsBaseUrl, SECURITY_PATH, path).toString();
    }
    
    private String urlFromPath(String path, String res) {
        return HTTPUtils.append(gsBaseUrl, SECURITY_PATH, path, "/", res).toString();
    }
    
    public RESTDataRules getDataRules() {
        return RESTDataRules.build(HTTPUtils.get(urlFromPath(DATA_PATH + ".xml"), auth));
    }
    
    public void addDataRules(GSDataRulesEncoder rules) {
        HTTPUtils.postXml(urlFromPath(DATA_PATH), rules.toString(), auth);
    }
    
    public void modifyDataRules(GSDataRulesEncoder rules) {
        HTTPUtils.putXml(urlFromPath(DATA_PATH), rules.toString(), auth);
    }
    
    public void deleteDataRule(String workspace, String layer, RuleType type) {
        HTTPUtils.delete(urlFromPath(DATA_PATH, resource(workspace, layer, type)), auth);
    }
}
