package it.geosolutions.geoserver.rest.gwc;

import it.geosolutions.geoserver.rest.GeoserverRESTTest;
import it.geosolutions.geoserver.rest.decoder.RESTGridset;
import it.geosolutions.geoserver.rest.encoder.gridsets.GSGridSetEncoder;
import it.geosolutions.geoserver.rest.publisher.GeoserverRESTArcGridTest;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GwcRestGridsetTest extends GeoserverRESTTest {
    private final static Logger LOGGER = LoggerFactory.getLogger(GwcRestGridsetTest.class);

    @Test
    public void testUpdateGridset() {
        GSGridSetEncoder gsGridSetEncoder = new GSGridSetEncoder("demo_3306", "这是一个测试的网格集");
        gsGridSetEncoder.setSRS(4326);
        gsGridSetEncoder.setTileSize(128, 128);
        gsGridSetEncoder.setCoords(-180, -90, 180, 90);
        gsGridSetEncoder.addLevel("demo:0", 0.701);
        gsGridSetEncoder.addLevel("demo:1", 0.301);
        gsGridSetEncoder.addLevel("demo:2", 0.101);
        gsGridSetEncoder.addLevel("demo:3", 0.021);

        boolean b = manager.getGeoWebCacheRest().updateGridSet(gsGridSetEncoder);
        Assert.assertTrue(b);
    }

    @Test
    public void testGetGridset() {
        RESTGridset gridset = manager.getGeoWebCacheRest().getGridset("demo_3306");
        Assert.assertNotNull(gridset);
        Assert.assertEquals("demo_3306", gridset.getName());
        Assert.assertEquals(4326, gridset.getSRS());

        double[] coords = gridset.getCoords();
        Assert.assertEquals(-180, coords[0], 0.000000000000001);
        Assert.assertEquals(-90, coords[1], 0.000000000000001);
        Assert.assertEquals(180, coords[2], 0.000000000000001);
        Assert.assertEquals(90, coords[3], 0.000000000000001);

        Assert.assertEquals(128, gridset.getTileHeight());
        Assert.assertEquals(128, gridset.getTileWidth());
        double[] resoutions = gridset.getResoutions();
        Assert.assertNotNull(resoutions);

        String[] scaleNames = gridset.getScaleNames();
        Assert.assertNotNull(scaleNames);
    }

    @Test
    public void testDeleteGridset() {
        boolean b = manager.getGeoWebCacheRest().deleteGridset("demo_3306");
        Assert.assertTrue(b);
    }
}
